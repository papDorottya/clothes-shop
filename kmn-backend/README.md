# [Quick start](#quick-start)

## Install Dependencies
npm install

## Development
* `npm run build` - this command auto compiles ts code into js code
* `npm run start` - this command starts the server
* in your browser go to [http://localhost:4200]

## Push git changes
* `git add .` - this command adds all the file changes to git
* `git commit -m "message"` - this command assigns a message to the commit
* `git push` - this command pushes the changes to the repository

## Pull git changes
* `git pull` - this command gets the changes from the repository

