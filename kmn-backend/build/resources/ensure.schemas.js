"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = void 0;
const mongoose = require("mongoose");
const http_utils_1 = require("../utils/http.utils");
/**
 * Dynamic Module Loading
 */
exports.DB = {
    model: (name) => {
        try {
            require(`./${name.toLowerCase()}.schema`);
            return mongoose.model(name);
        }
        catch (error) {
            throw { status: http_utils_1.HTTP_STATUS.NOT_FOUND, message: `Unknown resource ${name}`, error: error };
        }
    }
};
//# sourceMappingURL=ensure.schemas.js.map