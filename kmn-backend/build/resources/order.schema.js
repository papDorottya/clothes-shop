"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderSchema = void 0;
const Mongoose = require("mongoose");
const orderStatus_enum_1 = require("./orderStatus.enum");
const schemas_1 = require("./schemas");
exports.OrderSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: schemas_1.Schemas.Order
    },
    userId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: schemas_1.Schemas.User
    },
    status: {
        type: String,
        enum: Object.values(orderStatus_enum_1.OrderStatusEnum),
        default: orderStatus_enum_1.OrderStatusEnum.InProgress
    },
    total: {
        type: Number,
        default: 0
    }
});
const OrderModel = Mongoose.model(schemas_1.Schemas.Order, exports.OrderSchema);
exports.default = OrderModel;
//# sourceMappingURL=order.schema.js.map