"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTypeEnum = void 0;
var UserTypeEnum;
(function (UserTypeEnum) {
    UserTypeEnum["Administrator"] = "Administrator";
    UserTypeEnum["Client"] = "Client";
})(UserTypeEnum = exports.UserTypeEnum || (exports.UserTypeEnum = {}));
//# sourceMappingURL=userType.enum copy.js.map