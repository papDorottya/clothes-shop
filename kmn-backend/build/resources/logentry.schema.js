"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogEntrySchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.LogEntrySchema = new Mongoose.Schema({
    msg: {
        type: String
    },
    level: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    res: {
        type: Object
    },
    req: {
        type: Object
    }
});
const LogEntryModel = Mongoose.model(schemas_1.Schemas.LogEntry, exports.LogEntrySchema);
exports.default = LogEntryModel;
//# sourceMappingURL=logentry.schema.js.map