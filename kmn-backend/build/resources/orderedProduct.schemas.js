"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderedProductSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.OrderedProductSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: schemas_1.Schemas.OrderedProduct
    },
    productId: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.Product }
    ],
    orderId: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.Order }
    ],
    quantity: {
        type: Number,
        default: 0
    }
});
const UserModel = Mongoose.model(schemas_1.Schemas.OrderedProduct, exports.OrderedProductSchema);
exports.default = UserModel;
//# sourceMappingURL=orderedProduct.schemas.js.map