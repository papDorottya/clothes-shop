"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.ProductSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: 'Product'
    },
    id: String,
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: String,
    price: Number,
    isAvailable: Boolean,
    categoryId: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.ProductCategory }
    ]
});
exports.ProductSchema.pre('save', function (next) {
    this.id = this._id;
    next();
});
const UserModel = Mongoose.model(schemas_1.Schemas.Product, exports.ProductSchema);
exports.default = UserModel;
//# sourceMappingURL=product.schemas.js.map