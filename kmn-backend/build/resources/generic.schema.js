"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenericSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.GenericSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: 'Generic'
    },
    id: String,
    testField: {
        type: String,
        required: true,
        unique: true
    },
    genericNumber: {
        type: Number,
        required: true
    },
    genericUnk: String
});
exports.GenericSchema.pre('save', function (next) {
    this.id = this._id;
    next();
});
const GenericModel = Mongoose.model(schemas_1.Schemas.Generic, exports.GenericSchema);
exports.default = GenericModel;
//# sourceMappingURL=generic.schema.js.map