"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
const productCategory_enum_1 = require("./productCategory.enum");
exports.ProductSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: schemas_1.Schemas.Product
    },
    name: {
        type: String,
        required: true
    },
    description: String,
    price: Number,
    isAvailable: Boolean,
    category: {
        type: String,
        enum: Object.values(productCategory_enum_1.ProductCategoryEnum),
        default: productCategory_enum_1.ProductCategoryEnum.Others
    },
    img: String
});
const ProductModel = Mongoose.model(schemas_1.Schemas.Product, exports.ProductSchema);
exports.default = ProductModel;
//# sourceMappingURL=product.schema.js.map