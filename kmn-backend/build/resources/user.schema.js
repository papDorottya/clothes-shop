"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
const userType_enum_1 = require("./userType.enum");
exports.UserSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: schemas_1.Schemas.User
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    type: {
        type: String,
        enum: Object.values(userType_enum_1.UserTypeEnum),
        default: userType_enum_1.UserTypeEnum.Client
    }
});
const UserModel = Mongoose.model(schemas_1.Schemas.User, exports.UserSchema);
exports.default = UserModel;
//# sourceMappingURL=user.schema.js.map