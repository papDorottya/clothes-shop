"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderProductSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.OrderProductSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: schemas_1.Schemas.OrderProduct
    },
    productId: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.Product }
    ],
    orderId: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.Order }
    ],
    quantity: {
        type: Number,
        default: 0
    }
});
const UserModel = Mongoose.model(schemas_1.Schemas.OrderProduct, exports.OrderProductSchema);
exports.default = UserModel;
//# sourceMappingURL=orderProduct.schema.js.map