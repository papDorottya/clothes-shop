"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Schemas = void 0;
exports.Schemas = {
    User: "User",
    LogEntry: "LogEntry",
    Product: "Product",
    Order: "Order",
    OrderProduct: "OrderProduct"
};
//# sourceMappingURL=schemas.js.map