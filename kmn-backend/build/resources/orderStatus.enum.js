"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderStatusEnum = void 0;
var OrderStatusEnum;
(function (OrderStatusEnum) {
    OrderStatusEnum["InProgress"] = "InProgress";
    OrderStatusEnum["Delivered"] = "Delivered";
    OrderStatusEnum["Finished"] = "Finished";
})(OrderStatusEnum = exports.OrderStatusEnum || (exports.OrderStatusEnum = {}));
//# sourceMappingURL=orderStatus.enum.js.map