"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.OrderSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: 'Order'
    },
    userId: {
        type: Mongoose.Schema.Types.ObjectId,
        ref: schemas_1.Schemas.User
    },
    productIds: [
        { type: Mongoose.Schema.Types.ObjectId, ref: schemas_1.Schemas.Product }
    ]
});
exports.OrderSchema.pre('save', function (next) {
    next();
});
const OrderModel = Mongoose.model(schemas_1.Schemas.Order, exports.OrderSchema);
exports.default = OrderModel;
//# sourceMappingURL=order.schemas.js.map