"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductCategorySchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.ProductCategorySchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: 'ProductCategory'
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
});
exports.ProductCategorySchema.pre('save', function (next) {
    next();
});
const UserModel = Mongoose.model(schemas_1.Schemas.ProductCategory, exports.ProductCategorySchema);
exports.default = UserModel;
//# sourceMappingURL=productCategory.schema.js.map