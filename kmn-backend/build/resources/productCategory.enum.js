"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductCategoryEnum = void 0;
var ProductCategoryEnum;
(function (ProductCategoryEnum) {
    ProductCategoryEnum["Jeans"] = "Jeans";
    ProductCategoryEnum["Sweatshirts"] = "Sweatshirts";
    ProductCategoryEnum["Dresses"] = "Dresses";
    ProductCategoryEnum["Jackets"] = "Jackets";
    ProductCategoryEnum["Shoes"] = "Shoes";
    ProductCategoryEnum["Others"] = "Others";
})(ProductCategoryEnum = exports.ProductCategoryEnum || (exports.ProductCategoryEnum = {}));
//# sourceMappingURL=productCategory.enum.js.map