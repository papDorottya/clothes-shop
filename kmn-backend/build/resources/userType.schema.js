"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userTypeSchema = void 0;
const Mongoose = require("mongoose");
const schemas_1 = require("./schemas");
exports.userTypeSchema = new Mongoose.Schema({
    resourceType: {
        type: String,
        default: 'UserType'
    },
    name: {
        type: String,
        required: true,
    }
});
exports.userTypeSchema.pre('save', function (next) {
    next();
});
const UserTypeModel = Mongoose.model(schemas_1.Schemas.UserType, exports.userTypeSchema);
exports.default = UserTypeModel;
//# sourceMappingURL=userType.schema.js.map