"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketIO = void 0;
exports.SocketIO = (function () {
    let io;
    let clients = [];
    return {
        emit: emit,
        connectWebSocket: connectWebSocket
    };
    function connectWebSocket(server) {
        io = require('socket.io')(server, {
            cors: {
                origin: "*",
                methods: ["GET", "POST", "PUT"]
            }
        });
        io.on('connection', (newClient) => {
            clients.push(newClient);
        });
    }
    function emit(channel, value) {
        clients.forEach(c => c.emit(channel, value));
    }
})();
//# sourceMappingURL=socket.service.js.map