"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.errorHandler = void 0;
const logger_1 = require("./logger");
const db_mongoose_1 = require("./db.mongoose");
const http_utils_1 = require("./http.utils");
const logger = logger_1.getLogger('Error Handler');
function errorHandler() {
    return (ctx, next) => __awaiter(this, void 0, void 0, function* () {
        try {
            //check database status
            let connected = false;
            if (db_mongoose_1.MongooseDB.status() === db_mongoose_1.DATABASE_STATUS.CONNECTED)
                connected = true;
            if (!connected)
                ctx.throw(http_utils_1.HTTP_STATUS.INTERNAL_SERVER_ERROR, "Database connection is down!");
            yield next();
            // Handle 404 upstream.
            let status = ctx.status || http_utils_1.HTTP_STATUS.NOT_FOUND;
            if (status === http_utils_1.HTTP_STATUS.NOT_FOUND)
                ctx.throw(http_utils_1.HTTP_STATUS.NOT_FOUND, "Uh-Oh! That page is in /dev/null");
        }
        catch (error) {
            logger.debug(error);
            ctx.status = error.status;
            ctx.body = error.message;
        }
    });
}
exports.errorHandler = errorHandler;
//# sourceMappingURL=error.handler.js.map