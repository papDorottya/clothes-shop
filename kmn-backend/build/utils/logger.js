"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLogger = void 0;
const server_config_1 = require("../config/server-config");
const ensure_schemas_1 = require("../resources/ensure.schemas");
const schemas_1 = require("../resources/schemas");
const bunyan = require('bunyan');
const LogEntryStream = require('bunyan-mongodb-stream')({ model: ensure_schemas_1.DB.model(schemas_1.Schemas.LogEntry) });
const bunyanStreams = [
    {
        level: server_config_1.getConfig().loggerConfig.level,
        stream: process.stdout
    },
    {
        level: server_config_1.getConfig().loggerConfig.level,
        path: server_config_1.getConfig().loggerConfig.pathFile
    },
    {
        stream: LogEntryStream
    }
];
function getLogger(tagName) {
    return bunyan.createLogger({
        name: tagName,
        streams: bunyanStreams
    });
}
exports.getLogger = getLogger;
//# sourceMappingURL=logger.js.map