"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.authJwt = exports.jwtConfig = void 0;
const server_config_1 = require("../config/server-config");
let jwt = require('jwt-simple');
exports.jwtConfig = { secret: server_config_1.getConfig().authConfig.authSecret };
class AuthUtils {
    createToken(user, ctx) {
        return jwt.encode({
            username: user.username,
            _id: user._id
        }, exports.jwtConfig.secret, null, { expiresIn: 60 * 60 * 60 * 60 /*secs*/ });
    }
    decodeToken(token) {
        return jwt.decode(token, exports.jwtConfig.secret);
    }
}
exports.authJwt = new AuthUtils();
//# sourceMappingURL=authenticate.js.map