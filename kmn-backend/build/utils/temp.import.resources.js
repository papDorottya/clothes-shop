"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.populateDB = void 0;
const logger_1 = require("./logger");
const schemas_1 = require("../resources/schemas");
const ensure_schemas_1 = require("../resources/ensure.schemas");
const userType_enum_1 = require("../resources/userType.enum");
const productCategory_enum_1 = require("../resources/productCategory.enum");
const logger = logger_1.getLogger('temp.import.resources');
const path = require('path');
const fs = require('fs');
function populateDB() {
    return __awaiter(this, void 0, void 0, function* () {
        // Process
        try {
            yield createUsers();
            yield createProducts();
            yield createOrders();
            yield createOrderProducts();
            logger.info("Populate db finished.");
        }
        catch (er) {
            logger.info(er);
        }
    });
}
exports.populateDB = populateDB;
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
class TipProdus {
}
function createUsers() {
    return __awaiter(this, void 0, void 0, function* () {
        let UserModel = ensure_schemas_1.DB.model(schemas_1.Schemas.User);
        yield UserModel.deleteMany({});
        yield new UserModel({
            username: "admin",
            password: "admin",
            firstname: "Gabi",
            lastname: "Gaben",
            email: "gaba@gmail.com",
            phone: "0722 222 234",
            type: userType_enum_1.UserTypeEnum.Administrator
        }).save();
        yield new UserModel({
            username: "client",
            password: "client",
            firstname: "client",
            lastname: "client",
            email: "client@gmail.com",
            phone: "12345678",
        }).save();
    });
}
function createProducts() {
    return __awaiter(this, void 0, void 0, function* () {
        let ProductModel = ensure_schemas_1.DB.model(schemas_1.Schemas.Product);
        yield ProductModel.deleteMany({});
        const products = {
            Jeans: {
                names: ["nike", "adidas", "converse", "puma"],
                description: "100% cotton",
                category: productCategory_enum_1.ProductCategoryEnum.Jeans
            },
            Sweatshirts: {
                names: ["nike", "adidas", "converse", "puma"],
                description: "90% cotton",
                category: productCategory_enum_1.ProductCategoryEnum.Sweatshirts
            },
            Dresses: {
                names: ["nike", "adidas"],
                description: "70% cotton",
                category: productCategory_enum_1.ProductCategoryEnum.Dresses
            },
            Jackets: {
                names: ["nike", "adidas"],
                description: "50% cotton",
                category: productCategory_enum_1.ProductCategoryEnum.Jackets
            },
            Shoes: {
                names: ["nike", "adidas", "converse", "puma"],
                description: "100% cotton",
                category: productCategory_enum_1.ProductCategoryEnum.Shoes
            }
        };
        const directoryPath = path.join(__dirname, '../../Pictures');
        let pictures = yield getPictures(directoryPath);
        for (let i = 1; i < 10; i++) {
            yield Object.keys(products)
                .forEach((tip) => __awaiter(this, void 0, void 0, function* () {
                var e_1, _a;
                let tipProdus = products[tip];
                let imagePath = path.join(directoryPath, pictures[Math.floor(Math.random() * pictures.length)]);
                try {
                    for (var _b = __asyncValues(tipProdus.names), _c; _c = yield _b.next(), !_c.done;) {
                        let name = _c.value;
                        yield new ProductModel({
                            name: `${name}${i}`,
                            description: tipProdus.description,
                            price: getRandomInt(300, 500),
                            isAvailable: Math.random() <= 0.5,
                            category: tipProdus.category,
                            img: "data:image/jpeg;base64," + (yield readFile(imagePath))
                        }).save();
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) yield _a.call(_b);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
            }));
        }
    });
}
function getPictures(directoryPath) {
    return __awaiter(this, void 0, void 0, function* () {
        //passsing directoryPath and callback function
        return yield new Promise((resolve) => {
            fs.readdir(directoryPath, function (err, files) {
                //handling error
                if (err) {
                    return console.log('Unable to scan directory: ' + err);
                }
                resolve(files);
            });
        });
    });
}
function createOrders() {
    return __awaiter(this, void 0, void 0, function* () {
        let OrderModel = ensure_schemas_1.DB.model(schemas_1.Schemas.Order);
        yield OrderModel.deleteMany({});
    });
}
function readFile(imagePath) {
    return __awaiter(this, void 0, void 0, function* () {
        return yield new Promise((resolve) => {
            var bitmap = fs.readFileSync(imagePath);
            resolve(new Buffer(bitmap).toString('base64'));
        });
    });
}
function createOrderProducts() {
    return __awaiter(this, void 0, void 0, function* () {
        let OrderProductModel = ensure_schemas_1.DB.model(schemas_1.Schemas.OrderProduct);
        yield OrderProductModel.deleteMany({});
    });
}
//# sourceMappingURL=temp.import.resources.js.map