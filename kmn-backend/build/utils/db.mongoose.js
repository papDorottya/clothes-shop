"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DATABASE_STATUS = exports.connectToDatabase = exports.MongooseDB = void 0;
const logger_1 = require("./logger");
const server_config_1 = require("../config/server-config");
exports.MongooseDB = (function () {
    var instance;
    var config = server_config_1.getConfig().databaseConfig;
    return {
        getInstance: getInstance,
        connectiontURL: `mongodb://${config.host}:${config.port}/${config.name}`,
        status: () => getInstance().connection.readyState
    };
    function getInstance() {
        if (!instance) {
            instance = require('mongoose');
        }
        return instance;
    }
})();
const logger = logger_1.getLogger("Mongo Database connection");
function connectToDatabase(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve) => {
            exports.MongooseDB.getInstance().connection
                .on('error', (error) => {
                logger.error(`Unable to connect to the database: ${error}`);
                setTimeout(() => exports.MongooseDB.getInstance().connect(url), 500);
            })
                .on('open', () => {
                logger.info(`Database connection opened`);
                resolve(exports.MongooseDB.getInstance().connections[0]);
            })
                .on('close', () => {
                logger.info(`Database connection closed`);
            });
            exports.MongooseDB.getInstance().connect(url);
            logger.info('Connecting to the database ...');
        });
    });
}
exports.connectToDatabase = connectToDatabase;
exports.DATABASE_STATUS = {
    DISCONNECTED: 0,
    CONNECTED: 1,
    CONNECTING: 2,
    DISCONNECTING: 3
};
//# sourceMappingURL=db.mongoose.js.map