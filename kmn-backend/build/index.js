"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Koa = require("koa");
const routes_1 = require("./routes");
const server_config_1 = require("./config/server-config");
const logger_1 = require("./utils/logger");
const db_mongoose_1 = require("./utils/db.mongoose");
const error_handler_1 = require("./utils/error.handler");
const temp_import_resources_1 = require("./utils/temp.import.resources");
const socket_service_1 = require("./utils/socket.service");
const logger = logger_1.getLogger('Main');
const cors = require('@koa/cors');
const body = require('koa-better-body');
class MainServer {
    constructor() {
        logger.info('Start creating KOA Server');
        this.app = new Koa();
        this.port = server_config_1.getConfig().port;
        this.configMiddlewares();
        this.configRoutes();
    }
    configMiddlewares() {
        logger.info('Config Middlewares');
        this.logRequestTime();
        this.app.use(body({
            textLimit: '30mb',
            formLimit: '30mb',
            urlencodedLimit: '30mb',
            jsonLimit: '30mb',
            bufferLimit: '30mb',
        }));
        this.app.use(cors({
            origin: '*'
        }));
        this.app.use(error_handler_1.errorHandler());
    }
    logRequestTime() {
        this.app.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            const start = new Date();
            yield next();
            logger.info(`${ctx.method} ${ctx.url} - ${+new Date() - start}ms`);
        }));
    }
    configRoutes() {
        let mainRouter = new routes_1.MainRouter(null);
        this.app.use(mainRouter.routes())
            .use(mainRouter.allowedMethods());
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            this.server = require('http').Server(this.app.callback());
            socket_service_1.SocketIO.connectWebSocket(this.server);
            yield this.server.listen(this.port);
            logger.info(`Server started on port: ${this.port}`);
            yield db_mongoose_1.connectToDatabase(db_mongoose_1.MongooseDB.connectiontURL);
            yield temp_import_resources_1.populateDB();
        });
    }
}
new MainServer().start();
//# sourceMappingURL=index.js.map