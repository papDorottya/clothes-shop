"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicRouter = void 0;
const Router = require("koa-router");
const auth_router_1 = require("./auth.router");
const clientCreate_router_1 = require("./clientCreate.router");
const routes_1 = require("../routes");
class PublicRouter extends Router {
    constructor(args) {
        super(args);
        // public routes goes here
        this.use(routes_1.ROUTES.AUTH_URL, new auth_router_1.AuthRouter(null).routes());
        this.use(routes_1.ROUTES.CLIENT_URL, new clientCreate_router_1.ClientCreateRouter(null).routes());
    }
}
exports.PublicRouter = PublicRouter;
//# sourceMappingURL=index.js.map