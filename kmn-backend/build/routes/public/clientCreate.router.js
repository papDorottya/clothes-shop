"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientCreateRouter = void 0;
const Router = require("koa-router");
const routes_1 = require("../routes");
const http_utils_1 = require("../../utils/http.utils");
const ensure_schemas_1 = require("../../resources/ensure.schemas");
const schemas_1 = require("../../resources/schemas");
class ClientCreateRouter extends Router {
    constructor(args) {
        super(args);
        /*
         Create account on server.
         full Url: localhost:1234/client/create
         required: username, password, firstname, lastname, email, phone
         */
        this.post(routes_1.ROUTES.CLIENT_CREATE_URL, (ctx) => __awaiter(this, void 0, void 0, function* () {
            let reqBody = ctx.request.fields;
            if (!reqBody.username || !reqBody.password || !reqBody.firstname || !reqBody.lastname || !reqBody.email || !reqBody.phone) {
                throw { status: http_utils_1.HTTP_STATUS.BAD_REQUEST, message: "You must complete all the fields!" };
            }
            let errors = [];
            // check if user already exists
            let sameUser = yield ensure_schemas_1.DB.model(schemas_1.Schemas.User).findOne({ username: reqBody.username });
            let sameEmail = yield ensure_schemas_1.DB.model(schemas_1.Schemas.User).findOne({ email: reqBody.email });
            if (sameUser) {
                errors.push("The username already exists");
            }
            if (sameEmail) {
                errors.push("The email already exists");
            }
            if (errors.length > 0) {
                throw { status: http_utils_1.HTTP_STATUS.BAD_REQUEST, message: errors };
            }
            let UserModel = ensure_schemas_1.DB.model(schemas_1.Schemas.User);
            let client = yield new UserModel({
                username: reqBody.username,
                password: reqBody.password,
                firstname: reqBody.firstname,
                lastname: reqBody.lastname,
                email: reqBody.email,
                phone: reqBody.phone
            }).save();
            if (client) {
                ctx.status = http_utils_1.HTTP_STATUS.OK;
                ctx.body = { message: "Create account: success" };
            }
            else {
                throw { status: http_utils_1.HTTP_STATUS.BAD_REQUEST, message: "Try again later." };
            }
        }));
    }
}
exports.ClientCreateRouter = ClientCreateRouter;
//# sourceMappingURL=clientCreate.router.js.map