"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRouter = void 0;
const Router = require("koa-router");
const routes_1 = require("../routes");
const authenticate_1 = require("../../utils/authenticate");
const http_utils_1 = require("../../utils/http.utils");
const ensure_schemas_1 = require("../../resources/ensure.schemas");
const schemas_1 = require("../../resources/schemas");
class AuthRouter extends Router {
    constructor(args) {
        super(args);
        /*
         Login on server.
         full Url: localhost:1234/auth/login
         required: username, password.
         */
        this.post(routes_1.ROUTES.LOGIN_URL, (ctx) => __awaiter(this, void 0, void 0, function* () {
            let reqBody = ctx.request.fields;
            if (!reqBody.username || !reqBody.password) {
                throw { status: http_utils_1.HTTP_STATUS.BAD_REQUEST, message: "You must send the username and the password" };
            }
            let user = yield ensure_schemas_1.DB.model(schemas_1.Schemas.User).findOne({ username: reqBody.username });
            let password = reqBody.password;
            if (user && user.password === password) {
                ctx.status = http_utils_1.HTTP_STATUS.OK;
                ctx.body = {
                    token: authenticate_1.authJwt.createToken(user, ctx),
                    user: user
                };
            }
            else {
                throw { status: http_utils_1.HTTP_STATUS.UNAUTHORIZED, message: "The username or password don't match" };
            }
        }));
        /*
         Logout on server.
         full Url: localhost:1234/auth/logout
         */
        // Useless function
        this.post(routes_1.ROUTES.LOGOUT_URL, (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = { message: "Logout: success" };
        }));
    }
}
exports.AuthRouter = AuthRouter;
//# sourceMappingURL=auth.router.js.map