"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainRouter = void 0;
const Router = require("koa-router");
const index_1 = require("./public/index");
const index_2 = require("./private/index");
const authenticate_1 = require("../utils/authenticate");
__exportStar(require("./private"), exports);
__exportStar(require("./public"), exports);
__exportStar(require("./routes"), exports);
const koaJwt = require('koa-jwt');
const convert = require('koa-convert');
class MainRouter extends Router {
    constructor(args) {
        super(args);
        this.use(new index_1.PublicRouter(null).routes());
        this.use(convert(koaJwt(authenticate_1.jwtConfig))); // => we need a token to acceess this
        this.use(new index_2.PrivateRouter(null).routes());
    }
}
exports.MainRouter = MainRouter;
//# sourceMappingURL=index.js.map