"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpecificResourceRouter = void 0;
const Router = require("koa-router");
const http_utils_1 = require("../../utils/http.utils");
const ensure_schemas_1 = require("../../resources/ensure.schemas");
const schemas_1 = require("../../resources/schemas");
const socket_service_1 = require("../../utils/socket.service");
class SpecificResourceRouter extends Router {
    constructor(args) {
        super(args);
        /*
         Get all orders for user with userId
         method: get
         full Url: localhost:1234/ResourceName/id
         ex: localhost:1234/User/584bda4a3530553ef4d20b5a
         */
        this.get('/Order/:userId', (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Order).findOne({ userId: ctx.params.userId });
        }));
        /*
        Get all products for a specific category
        method: get
        full Url: localhost:3000/Specific/Product/Category
        ex: localhost:3000/Specific/Product/Jeans
        */
        this.get('/Product/:category', (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Product).find({ category: ctx.params.category });
        }));
        /*
        Create a new order with a product list
        method: post
        full Url: localhost:3000/Specific/Order/create
        ex: localhost:3000/Specific/Order/create
        body: {userId: "1231923012", productIdsQuantities: {"1213123019231": 1,"123123123123": 2,"`2239298923932": 4}}
        */
        this.post('/Order/create', (ctx) => __awaiter(this, void 0, void 0, function* () {
            var e_1, _a, e_2, _b;
            const request = ctx.request;
            let OrderModel = ensure_schemas_1.DB.model(schemas_1.Schemas.Order);
            let ProductModel = ensure_schemas_1.DB.model(schemas_1.Schemas.Product);
            let OrderProductModel = ensure_schemas_1.DB.model(schemas_1.Schemas.OrderProduct);
            // calculate total price
            let totalPrice = 0;
            let productIdsQuantities = request.fields.productIdsQuantities;
            let ids = Object.keys(productIdsQuantities);
            try {
                for (var ids_1 = __asyncValues(ids), ids_1_1; ids_1_1 = yield ids_1.next(), !ids_1_1.done;) {
                    const id = ids_1_1.value;
                    const quantity = productIdsQuantities[id];
                    const product = yield ProductModel.findOne({ _id: id });
                    totalPrice += product.price * quantity;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (ids_1_1 && !ids_1_1.done && (_a = ids_1.return)) yield _a.call(ids_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            // create order
            let order = yield new OrderModel({
                total: totalPrice,
                userId: request.fields.userId
            }).save();
            try {
                // create ordered product entities
                for (var ids_2 = __asyncValues(ids), ids_2_1; ids_2_1 = yield ids_2.next(), !ids_2_1.done;) {
                    const id = ids_2_1.value;
                    const quantity = productIdsQuantities[id];
                    yield new OrderProductModel({
                        orderId: order._id,
                        productId: id,
                        quantity: quantity
                    }).save();
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (ids_2_1 && !ids_2_1.done && (_b = ids_2.return)) yield _b.call(ids_2);
                }
                finally { if (e_2) throw e_2.error; }
            }
            ctx.body = order;
            ctx.status = http_utils_1.HTTP_STATUS.CREATED;
            socket_service_1.SocketIO.emit('order event', order);
        }));
        /*
         Update order
         method: put
         full Url: localhost:3000/specifiic/Order
         ex: localhost:3000/specifiic/Order/584bda4a3530553ef4d20b5a
         */
        this.put('/Order/:id', (ctx) => __awaiter(this, void 0, void 0, function* () {
            let request = ctx.request;
            let OrderModel = ensure_schemas_1.DB.model(schemas_1.Schemas.Order);
            let order = yield OrderModel.findOneAndUpdate({ _id: ctx.params.id }, request.fields, { new: true });
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = order;
            socket_service_1.SocketIO.emit('order event', order);
        }));
        /*
         Get all user orders
         method: get
         full Url: localhost:3000/Specific/UserOrder/userId
         ex: localhost:3000/Specific/UserOrder/891276543567890
         */
        this.get('/UserOrder/:userId', (ctx) => __awaiter(this, void 0, void 0, function* () {
            var e_3, _c, e_4, _d;
            let user = yield ensure_schemas_1.DB.model(schemas_1.Schemas.User).findOne({ _id: ctx.params.userId });
            let orders = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Order).find({ userId: ctx.params.userId });
            let viewOrders = [];
            try {
                for (var orders_1 = __asyncValues(orders), orders_1_1; orders_1_1 = yield orders_1.next(), !orders_1_1.done;) {
                    const order = orders_1_1.value;
                    const products = [];
                    const orderProducts = yield ensure_schemas_1.DB.model(schemas_1.Schemas.OrderProduct).find({ orderId: order._id });
                    try {
                        for (var orderProducts_1 = (e_4 = void 0, __asyncValues(orderProducts)), orderProducts_1_1; orderProducts_1_1 = yield orderProducts_1.next(), !orderProducts_1_1.done;) {
                            const orderProduct = orderProducts_1_1.value;
                            const product = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Product).findOne({ _id: orderProduct.productId });
                            products.push(Object.assign({ quantity: orderProduct.quantity }, product._doc));
                        }
                    }
                    catch (e_4_1) { e_4 = { error: e_4_1 }; }
                    finally {
                        try {
                            if (orderProducts_1_1 && !orderProducts_1_1.done && (_d = orderProducts_1.return)) yield _d.call(orderProducts_1);
                        }
                        finally { if (e_4) throw e_4.error; }
                    }
                    const viewOrder = {
                        _id: order._id,
                        products: products,
                        status: order.status,
                        total: order.total,
                        username: user.username
                    };
                    viewOrders.push(viewOrder);
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (orders_1_1 && !orders_1_1.done && (_c = orders_1.return)) yield _c.call(orders_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
            ctx.body = viewOrders;
            ctx.status = http_utils_1.HTTP_STATUS.OK;
        }));
        /*
        Get all orders
        method: get
        full Url: localhost:3000/Specific/AllOrders
        ex: localhost:3000/Specific/AllOrders
        */
        this.get('/AllOrders', (ctx) => __awaiter(this, void 0, void 0, function* () {
            var e_5, _e, e_6, _f;
            let orders = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Order).find({});
            let viewOrders = [];
            try {
                for (var orders_2 = __asyncValues(orders), orders_2_1; orders_2_1 = yield orders_2.next(), !orders_2_1.done;) {
                    const order = orders_2_1.value;
                    let products = [];
                    let orderProducts = yield ensure_schemas_1.DB.model(schemas_1.Schemas.OrderProduct).find({ orderId: order._id });
                    try {
                        for (var orderProducts_2 = (e_6 = void 0, __asyncValues(orderProducts)), orderProducts_2_1; orderProducts_2_1 = yield orderProducts_2.next(), !orderProducts_2_1.done;) {
                            const orderProduct = orderProducts_2_1.value;
                            const product = yield ensure_schemas_1.DB.model(schemas_1.Schemas.Product).findOne({ _id: orderProduct.productId });
                            products.push(Object.assign({ quantity: orderProduct.quantity }, product._doc));
                        }
                    }
                    catch (e_6_1) { e_6 = { error: e_6_1 }; }
                    finally {
                        try {
                            if (orderProducts_2_1 && !orderProducts_2_1.done && (_f = orderProducts_2.return)) yield _f.call(orderProducts_2);
                        }
                        finally { if (e_6) throw e_6.error; }
                    }
                    let user = yield ensure_schemas_1.DB.model(schemas_1.Schemas.User).findOne({ _id: order.userId });
                    let viewOrder = {
                        _id: order._id,
                        products: products,
                        status: order.status,
                        total: order.total,
                        username: user.username
                    };
                    viewOrders.push(viewOrder);
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (orders_2_1 && !orders_2_1.done && (_e = orders_2.return)) yield _e.call(orders_2);
                }
                finally { if (e_5) throw e_5.error; }
            }
            ctx.body = viewOrders;
            ctx.status = http_utils_1.HTTP_STATUS.OK;
        }));
    }
}
exports.SpecificResourceRouter = SpecificResourceRouter;
//# sourceMappingURL=specific.url.resources.js.map