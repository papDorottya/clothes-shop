"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PrivateRouter = void 0;
const Router = require("koa-router");
const resource_router_1 = require("./resource.router");
const routes_1 = require("../routes");
const specific_url_resources_1 = require("./specific.url.resources");
class PrivateRouter extends Router {
    constructor(args) {
        super(args);
        //Private routes goes here
        this.use(routes_1.ROUTES.SPECIFIC_URL, new specific_url_resources_1.SpecificResourceRouter(null).routes());
        this.use(routes_1.ROUTES.RESOURCE_URL, new resource_router_1.ResourceRouter(null).routes());
    }
}
exports.PrivateRouter = PrivateRouter;
//# sourceMappingURL=index.js.map