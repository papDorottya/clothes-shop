"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResourceRouter = void 0;
const Router = require("koa-router");
const http_utils_1 = require("../../utils/http.utils");
const ensure_schemas_1 = require("../../resources/ensure.schemas");
/*
    Generic Router for crud operations on resources.
 */
class ResourceRouter extends Router {
    constructor(args) {
        super(args);
        /*
         Get a specific resource (by id)
         method: get
         full Url: localhost:3000/resource/ResourceName/id
         ex: localhost:3000/resource/Product/584bda4a3530553ef4d20b5a
         */
        this.get('/:resourceType/:id', (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(ctx.params.resourceType).findOne({ _id: ctx.params.id });
        }));
        /*
         Get all resources for a specific type
         method: get
         full Url: localhost:1234/ResourceName
         ex: localhost:3000/resource/Product
         */
        this.get('/:resourceType', (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(ctx.params.resourceType).find({});
        }));
        /*
         Create a resource on server base on request`s body
         method: post
         full Url: localhost:3000/resource/ResourceName
         ex: localhost:3000/resource/Role
         */
        this.post('/:resourceType', (ctx) => __awaiter(this, void 0, void 0, function* () {
            let Model = ensure_schemas_1.DB.model(ctx.params.resourceType);
            let request = ctx.request;
            ctx.status = http_utils_1.HTTP_STATUS.CREATED;
            ctx.body = yield new Model(request.fields).save();
        }));
        /*
       Get a specific resource (by ids)
       method: get
       full Url: localhost:3000/resource/ResourceName/ids
       ex: localhost:3000/resource/Order/ids
       body: {ids: [584bda4a3530553ef4d20b5a,584bda4a3530553ef4d20b5a,584bda4a3530553ef4d20b5a]}
       */
        this.post('/:resourceType/ids', (ctx) => __awaiter(this, void 0, void 0, function* () {
            let request = ctx.request;
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(ctx.params.resourceType).where('_id').in(request.fields.ids).exec();
        }));
        /*
         Update a resource on server base on request`s body
         method: put
         full Url: localhost:3000/resource/ResourceName
         ex: localhost:3000/resource/User/584bda4a3530553ef4d20b5a
         */
        this.put('/:resourceType/:id', (ctx) => __awaiter(this, void 0, void 0, function* () {
            let request = ctx.request;
            ctx.status = http_utils_1.HTTP_STATUS.OK;
            ctx.body = yield ensure_schemas_1.DB.model(ctx.params.resourceType).findOneAndUpdate({ _id: ctx.params.id }, request.fields, { new: true });
            // ctx.status = httpStatus.CREATED; // TODO: set this if created
        }));
        /*
         Delete a resource on server base on id provided in url.
         method: delete
         full Url: localhost:3000/resource/ResourceName/id
         ex: localhost:3000/resource/Organization/584bda4a3530553ef4d20b5a
         */
        this.delete('/:resourceType/:id', (ctx) => __awaiter(this, void 0, void 0, function* () {
            ctx.status = http_utils_1.HTTP_STATUS.OK; // if 204 no content returned, body will be empty on client side...
            ctx.body = yield ensure_schemas_1.DB.model(ctx.params.resourceType).findOneAndRemove({ _id: ctx.params.id });
        }));
    }
}
exports.ResourceRouter = ResourceRouter;
//# sourceMappingURL=resource.router.js.map