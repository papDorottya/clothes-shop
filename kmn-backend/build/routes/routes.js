"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROUTES = void 0;
exports.ROUTES = {
    ALL: '/',
    LOGIN_URL: '/login',
    LOGOUT_URL: '/logout',
    AUTH_URL: '/auth',
    CLIENT_URL: '/client',
    CLIENT_CREATE_URL: '/create',
    SPECIFIC_URL: '/specific',
    RESOURCE_URL: '/resource',
    DROP_URL: '/drop'
};
//# sourceMappingURL=routes.js.map