import {ServerConfiguration} from './ServerConfiguration';

export function getConfig(version:any = null):ServerConfiguration
{
    return require('../../server-config.json');
}
